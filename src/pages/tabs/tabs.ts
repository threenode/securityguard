import { Component } from '@angular/core';

import { InicioPage } from '../inicio/inicio';
import { CarnetsPage } from '../carnets/carnets';
import { PlacasPage } from '../placas/placas';
import { EstadisticasPage } from '../estadisticas/estadisticas';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  public tabs: any;

  constructor(
  ) {
    this.tabs = [
      { title: "Inicio",        root: InicioPage,       icon: "home" },
      { title: "Carnets",       root: CarnetsPage,      icon: "card" },
      { title: "Placas",        root: PlacasPage,       icon: "car" },
      { title: "Estadisticas",  root: EstadisticasPage, icon: "stats" },
    ];

  }

}
