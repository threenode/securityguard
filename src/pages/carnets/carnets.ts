import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';

import { Observable, Subscription } from 'rxjs/Rx';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

import { ZBar } from 'ionic-native';
declare var jsQR;

@Component({
  selector: 'page-carnets',
  templateUrl: 'carnets.html',
})
export class CarnetsPage {
  @ViewChild("video")  video:  ElementRef;
  @ViewChild("canvas") canvas: ElementRef;
  private canvasContext:       CanvasRenderingContext2D;
  private mediaStream:         any;
  private requestAnimationID:  any;
  private scanInterval:        Subscription;

  public carnets:   FirebaseListObservable<any>;
  public entrada:   boolean;
  public isCordova: boolean;

  constructor(
    public navCtrl:   NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public af:        AngularFire,
    public plt:        Platform
  ) {
    this.carnets = af.database.list('/carnets');
    this.entrada = true;
  }


  ionViewDidLoad() {
    this.plt.ready().then((readySource) => {
      this.isCordova = this.plt.is("cordova");
      // this.isCordova = false;
    });
  }


  ionViewDidLeave(){
    this.clean();
  }


  clean(){
    if ( this.mediaStream && this.mediaStream.getVideoTracks()[0] && this.requestAnimationID && this.canvasContext){
      this.mediaStream.getVideoTracks()[0].stop();
      cancelAnimationFrame(this.requestAnimationID);
      this.scanInterval.unsubscribe();
      this.canvasContext.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    }
  }


  scanCode(){
    this.clean();
    // COMPORTAMIENTO EN MOVIL
    if (this.isCordova) {
      ZBar.scan({
        text_title:        "Escaniando Codigo...",
        text_instructions: "Apunta la camara a un codigo de barras/QR."
      })
      .then(result => {
        this.alertMsg("Escaneo QR", "Lectura exitosa<br> <b>" + result + "</b>");
        let datos = result.split(";");
        console.log(datos);
        this.carnets.push({
          nombre:  datos[0],
          cedula:  datos[1],
          carrera: datos[2],
          sede:    datos[3],
          vence:   datos[4],
          fecha:   new Date().toString(),
          entrada: this.entrada,
        });
      });
    } else {
      // EN CASO DE WEB
      console.log("NO CORDOVA");

      let width  = parseInt(this.canvas.nativeElement.style.width);
      let height = parseInt(this.canvas.nativeElement.style.height);
      this.canvas.nativeElement.width = width;
      this.canvas.nativeElement.height = height;

      this.canvasContext = this.canvas.nativeElement.getContext('2d');

      if (navigator.getUserMedia) {

        let vds: Array<any>; // VIDEO DEVICES DISPONIBLES
        navigator.mediaDevices.enumerateDevices().then( devices => {
          vds = devices.filter( d => {
            if (d.kind == "videoinput")
              return d;
          });

          let constraints =  {
            video:    true,
            // deviceId: { exact: vds[0].deviceId },
            facingMode: { exact: "environment" }
          };
          // CAPTURAR STREAM DE VICEO
          navigator.getUserMedia(constraints, (stream) => {
            this.video.nativeElement.src = window.URL.createObjectURL(stream) || stream;
            this.mediaStream = stream;
          }, error => console.log("ERROR: ", error));
          // CAPTURAR STREAM DE VICEO
        });


        // REQUESTANIMATION/CANVAS REFRESH
        let tick = () => {
          this.requestAnimationID = requestAnimationFrame(tick);
          if (this.video.nativeElement.readyState === this.video.nativeElement.HAVE_ENOUGH_DATA)
            this.canvasContext.drawImage(this.video.nativeElement, 0, 0, width, height);
        }
        this.requestAnimationID = requestAnimationFrame(tick);
        // REQUESTANIMATION/CANVAS REFRESH


        // ANALISIS DE FRAME CADA 1S
        this.scanInterval = Observable.interval(1000).subscribe(x => {

          let imageData = this.canvasContext.getImageData(0, 0, width, height);
          let decoded   = jsQR.decodeQRFromImage(imageData.data, imageData.width, imageData.height);

          if(decoded) {
            let datos = decoded.split(";");
            alert(decoded);
            this.alertMsg("Escaneo QR", "Lectura exitosa<br> <b>" + datos.join("<br>") + "</b>");
            this.carnets.push({
              nombre:  datos[0],
              cedula:  datos[1],
              carrera: datos[2],
              sede:    datos[3],
              vence:   datos[4],
              fecha:   new Date().toString(),
              entrada: this.entrada,
            });
          }
        });
        // ANALISIS DE FRAME CADA 1S
      }
    }

  }

  alertMsg(title, msg) {
    let alert = this.alertCtrl.create({
      title:    title,
      subTitle: msg,
    });
    alert.present();
  }

}
