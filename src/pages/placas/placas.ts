import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, AlertController, Platform, LoadingController } from 'ionic-angular';

// import { Observable, Subscription } from 'rxjs/Rx';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { MyService } from '../../providers/my-service'

// import { CameraPreview, CameraPreviewRect } from 'ionic-native';
// declare var CameraPreview: any;

@Component({
  selector:    'page-placas',
  templateUrl: 'placas.html',
  providers: [ MyService ]
})
export class PlacasPage {
  @ViewChild("video")  video:          ElementRef;
  @ViewChild("canvas") canvas:         ElementRef;
  @ViewChild("canvasTest") canvasTest: ElementRef;
  private canvasContext:               CanvasRenderingContext2D;
  private mediaStream:                 any;
  private requestAnimationID:          any;

  public entrada:   boolean;
  public isCordova: boolean;
  public placas:    FirebaseListObservable < any > ;

  constructor(
    public navCtrl:     NavController,
    public navParams:   NavParams,
    public alertCtrl:   AlertController,
    public af:          AngularFire,
    public plt:         Platform,
    public myService:   MyService,
    public loadingCtrl: LoadingController
  ) {
    this.placas  = this.af.database.list('/placas');   
    this.entrada = true;
  }


  // AL CREAR LA VISTA
  ionViewDidLoad() {
    this.plt.ready().then((readySource) => {
      // this.isCordova = this.plt.is("cordova");
      this.isCordova = false;
    });
  }
  // AL CREAR LA VISTA

  //  AL DEJAR LA VISTA
  ionViewDidLeave(){
    this.stopAll();
  }
  //  AL DEJAR LA VISTA

  // AL ENTRAR A LA VISTA
  ionViewDidEnter(){
    if (!this.isCordova) {
      this.startCameraStream();
    }
  }
  // AL ENTRAR A LA VISTA


  // DETENER TODAS LAS INSTANCIAS
  stopAll(){
    if ( this.mediaStream && this.mediaStream.getVideoTracks()[0] && this.requestAnimationID && this.canvasContext){
      this.mediaStream.getVideoTracks()[0].stop();
      cancelAnimationFrame(this.requestAnimationID);
      // this.scanInterval.unsubscribe();
      this.canvasContext.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    }
  }
  // DETENER TODAS LAS INSTANCIAS


  // SCANEAR FRAME  BUSCANDO PLACA
  scanPlate() {
    if (this.isCordova) {

    } else {
      let imageURL = this.canvas.nativeElement.toDataURL("image/jpeg");
      this.drawPolyImage(imageURL);

      let loading = this.loadingCtrl.create()
      loading.present();

      this.myService.recognizePlate(imageURL).subscribe( data => {
        loading.dismiss();

        if (data.results && data.results.length){
          let msg = "Placa: " + data.results[0].plate + "<br>" +
          "Fidelidad: " +  data.results[0].confidence;
          this.alertMsg("Escaneo de placas de Vehiculos", msg);

          this.drawPolyImage(imageURL, data.results[0].coordinates).then( () => {
            let datos_carros = {
              placa:   data.results[0].plate,
              fecha:   new Date().toString(),
              entrada: this.entrada,
              carro: {
                tipo:   data.results[0].vehicle.body_type[0].name,
                image:  this.canvasTest.nativeElement.toDataURL("image/jpeg"),
                color:  data.results[0].vehicle.color[0].name,
                marca:  data.results[0].vehicle.make[0].name,
                modelo: data.results[0].vehicle.make_model[0].name
              },
              autorizado: ( data.results[0].confidence < 60 ) ? false : true,
            };

            this.placas.push(datos_carros);  //  SUBIENDO DATOS

          });
        } else {
          this.alertMsg("Escaneo de placas de Vehiculos", "no hubo resultados");
        }
      });
    }
  }
  // SCANEAR FRAME  BUSCANDO PLACA


  // EMPEZAR STREAMING DE CAMARA
  startCameraStream(){
    let width  = parseInt(this.canvas.nativeElement.style.width);
    let height = parseInt(this.canvas.nativeElement.style.height);
    this.canvas.nativeElement.width = width;
    this.canvas.nativeElement.height = height;

    this.canvasContext = this.canvas.nativeElement.getContext('2d');

    if (navigator.getUserMedia) {
      // CAPTURAR STREAM DE VICEO
      let constraints =  {
        video: true,
        facingMode: { exact: "environment" }
      };
      navigator.getUserMedia(constraints, (stream) => {
        this.video.nativeElement.src = window.URL.createObjectURL(stream) || stream;
        this.mediaStream = stream;
      }, error => console.log("ERROR: ", error));
      // CAPTURAR STREAM DE VICEO
    

      // REQUESTANIMATION/CANVAS REFRESH
      let tick = () => {
        this.requestAnimationID = requestAnimationFrame(tick);
        if (this.video.nativeElement.readyState === this.video.nativeElement.HAVE_ENOUGH_DATA)
          this.canvasContext.drawImage(this.video.nativeElement, 0, 0, width, height);
      }
      this.requestAnimationID = requestAnimationFrame(tick);
      // REQUESTANIMATION/CANVAS REFRESH
    }
  }
  // EMPEZAR STREAMING DE CAMARA
 

  // DIBUJAR IMAGEN Y POLIGON EN 2 CANVAS
  drawPolyImage(imageURL, polygonCoors = null){
    return new Promise( (resolve, reject) => {
      let context = this.canvasTest.nativeElement.getContext('2d');
      context.clearRect(0, 0, this.canvasTest.nativeElement.width, this.canvasTest.nativeElement.height);
        
      let img = new Image();
      img.onload = (e) => {
        context.drawImage(img, 0, 0, this.canvasTest.nativeElement.width, this.canvasTest.nativeElement.height);
        
        if (polygonCoors){
          context.strokeStyle = 'rgba(0, 0, 200, 0.75)';
          context.beginPath();

          context.moveTo(polygonCoors[0].x, polygonCoors[0].y);
          for(let i = 1 ; i < polygonCoors.length ; i++)
            context.lineTo( polygonCoors[i].x , polygonCoors[i].y )
          
          context.lineWidth = 5;
          context.closePath();
          context.stroke();
        }
        resolve();
      };
      img.src= imageURL;
    });
  }
  // DIBUJAR IMAGEN Y POLIGON EN 2 CANVAS


  //  MOSTRAR UN ALERT CON MENSAGE
  alertMsg(title, msg) {
    let alert = this.alertCtrl.create({
      title:    title,
      subTitle: msg,
    });
    alert.present();
  }
  //  MOSTRAR UN ALERT CON MENSAGE
    
}
