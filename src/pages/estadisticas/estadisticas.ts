import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { AngularFire, FirebaseListObservable } from 'angularfire2';

import { Chart } from 'chart.js';

@Component({
  selector: 'page-estadisticas',
  templateUrl: 'estadisticas.html'
})
export class EstadisticasPage {
  public carnets:      FirebaseListObservable<any[]>;
  public carnetsQuery: string;
  public placas:       FirebaseListObservable<any>;
  public placasQuery:  string;
  public estadisticas: any;

  public barChart: any;
  @ViewChild("barCanvas") barCanvas: ElementRef;


  // CONSTRUCTOR
  constructor(
    public navCtrl:   NavController,
    public navParams: NavParams,
    public af:        AngularFire,
    public loadingCtrl: LoadingController
  ) {
      let loader = this.loadingCtrl.create();
      loader.present();

      this.carnets = this.af.database.list('/carnets');
      this.placas  = this.af.database.list('/placas');
      
      this.carnets.subscribe( data => {
        this.placas.subscribe(data => {
          loader.dismiss()
        })
      });

      this.estadisticas = "carnets"; // SEGMENTO INICIAL
      this.carnetsQuery = "";
      this.placasQuery  = ""; 
  }
  // CONSTRUCTOR



  // VISTA CARGADA
  ionViewDidLoad() {
    this.prepareCharts();
  }
  // VISTA CARGADA


  // FILTRADO DE CARNETS
  filterCarnets(carnet){
    // SE PUEDE MEJORAR XD PERO FUNCIONA
    if ( this.carnetsQuery ){
      if (!( 
        carnet.cedula .toLowerCase().indexOf( this.carnetsQuery.toLowerCase() )           !== -1 ||
        carnet.nombre .toLowerCase().indexOf( this.carnetsQuery.toLowerCase() )           !== -1 ||
        carnet.carrera.toLowerCase().indexOf( this.carnetsQuery.toLowerCase() )           !== -1 ||
        carnet.fecha  .toLowerCase().indexOf( this.carnetsQuery.toLowerCase() )           !== -1 ||
        carnet.sede   .toLowerCase().indexOf( this.carnetsQuery.toLowerCase() )           !== -1 ||
        carnet.vence  .toLowerCase().indexOf( this.carnetsQuery.toLowerCase() )           !== -1 ||      
        this.boolToTipoEntrada(carnet.entrada).indexOf( this.carnetsQuery.toLowerCase() ) !== -1 
      )){
        return true;
      }
    }
    return false;
    // SE PUEDE MEJORAR XD
  }
  // FILTRADO DE CARNETS

  
  //  FILTRADO DE PLACAS
  filterPlacas(placa){
    // SE PUEDE MEJORAR XD PERO FUNCIONA
    if ( this.placasQuery ){
      if (!( 
        placa.placa.toLowerCase().indexOf( this.placasQuery.toLowerCase() )                   !== -1 ||
        placa.fecha.toLowerCase().indexOf( this.placasQuery.toLowerCase() )                   !== -1 ||

        this.boolToTipoEntrada(placa.entrada)      .indexOf( this.placasQuery.toLowerCase() ) !== -1 ||
        this.boolToTipoAutorizado(placa.autorizado).indexOf( this.placasQuery.toLowerCase() ) !== -1 ||

        placa.carro.color.toLowerCase() .indexOf( this.placasQuery.toLowerCase() )            !== -1 ||
        placa.carro.marca.toLowerCase() .indexOf( this.placasQuery.toLowerCase() )            !== -1 ||
        placa.carro.modelo.toLowerCase().indexOf( this.placasQuery.toLowerCase() )            !== -1 ||
        placa.carro.tipo.toLowerCase()  .indexOf( this.placasQuery.toLowerCase() )            !== -1 
      )){
        return true;
      }
    }
    return false;
  }
  //  FILTRADO DE PLACAS


  // CHARTS
  prepareCharts(){
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: 'bar',
      data: {
          labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
          datasets: [{
              label: '# of Votes',
              data: [12, 19, 3, 5, 2, 3],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }
    });
  }
  // CHARTS
  
  
  // TYPO DE ENTRADA BOOLEANA A EQUIVALENTE EN TEXTO
  boolToTipoEntrada(bool: boolean): string{
    return bool ? "entrada" : "salida";
  }
  boolToTipoAutorizado(bool: boolean): string{
    return bool ? "autorizado" : "no autorizado";
  }
  // TYPO DE ENTRADA BOOLEANA A EQUIVALENTE EN TEXTO
}
