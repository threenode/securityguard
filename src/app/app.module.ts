import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AngularFireModule } from 'angularfire2';

import { TabsPage } from '../pages/tabs/tabs';
import { InicioPage } from '../pages/inicio/inicio';
import { CarnetsPage } from '../pages/carnets/carnets';
import { PlacasPage } from '../pages/placas/placas';
import { EstadisticasPage } from '../pages/estadisticas/estadisticas';

export const firebaseConfig = {
  apiKey: 'AIzaSyB0pXxa1Q6pPPz85ZSx6j0Mxe0AEyk1FTI',
  authDomain: 'securityguard-2dde2.firebaseapp.com',
  databaseURL: 'https://securityguard-2dde2.firebaseio.com',
  storageBucket: '',
  messagingSenderId: ''
};


@NgModule({
  declarations: [
    MyApp,

    TabsPage,
    InicioPage,
    CarnetsPage,
    PlacasPage,
    EstadisticasPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

    TabsPage,
    InicioPage,
    CarnetsPage,
    PlacasPage,
    EstadisticasPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
