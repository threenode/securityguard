import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams  } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class MyService {
  public openALRP_Key: string;
  public openALRP_secret: string;

  constructor(public http: Http) {
    this.openALRP_Key = "pk_b921fed93cdc6c820040031e";
    this.openALRP_secret = "sk_8cedb4419e60fb6adc279217";
  }

  recognizePlate(image64){
    let bodyString = image64.replace("data:image/jpeg;base64,", "");

    let params: URLSearchParams = new URLSearchParams();
    params.set('secret_key', this.openALRP_secret);
    params.set('recognize_vehicle', "1");
    params.set('country', "us");
    params.set('return_image', "0");
    params.set('topn', "10");
    params.set('image_bytes_prefix', "data:image/jpeg;base64,");

    let headers = new Headers({
      'Content-Type': 'multipart/form-data',
      'Accept':       'application/json'
    }); 
    let options = new RequestOptions({ headers: headers, search: params }); 

    return this.http.post("https://api.openalpr.com/v2/recognize_bytes", bodyString, options)
      .map( res => res.json()); 
  }

}
